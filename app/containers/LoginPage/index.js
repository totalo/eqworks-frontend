/**
 *
 * LoginPage
 *
 */

/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
// import { Helmet } from 'react-helmet';
// import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import { useFormik } from 'formik';
import * as Yup from 'yup';

import _has from 'lodash/has';
import _isEmpty from 'lodash/isEmpty';
// import { useInjectSaga } from 'utils/injectSaga';
// import { useInjectReducer } from 'utils/injectReducer';

import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Paper from '@material-ui/core/Paper';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import CopyRight from 'components/CopyRight';

// icon imports
// import LockOutlinedIcon from '@LockOutlined';
import LockOutlinedIcon from 'mdi-material-ui/LockOutline';

import Parse from 'utils/parse';

import LoginSidePanel from 'images/login_side_panel.png';
import makeSelectLoginPage from './selectors';
// import reducer from './reducer';
// import saga from './saga';
// import messages from './messages';

// console.log('LoginSidePanel', LoginSidePanel);
const useStyles = makeStyles(theme => ({
  root: {
    height: '100vh',
  },
  image: {
    // backgroundImage: 'url(https://source.unsplash.com/random)',
    backgroundImage: `url(${LoginSidePanel})`,
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'cover',
    backgroundPosition: 'center',
  },
  paper: {
    margin: theme.spacing(8, 4),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

const validationSchema = Yup.object().shape({
  username: Yup.string()
    .email('Invalid email')
    .required('Required'),
  password: Yup.string().required('Password required'),
});

function LoginPage(props) {
  // useInjectReducer({ key: 'loginPage', reducer });
  // useInjectSaga({ key: 'loginPage', saga });
  // const [uname, setUName] = React.useState();
  // const [pwd, setPwd] = React.useState();
  const classes = useStyles();

  if (Parse.User.current() && Parse.User.current().authenticated()) {
    props.history.push('/');
  }

  // function handleSubmit(e) {
  //   e.preventDefault();
  //   // console.log('uname', uname);
  //   // console.log('pwd', pwd);
  //   Parse.User.logIn(uname, pwd).then(r => {
  //     if (!_isEmpty(r.getSessionToken())) {
  //       props.history.push('/');
  //     } else {
  //       // TODO: show login error, authentication failed
  //     }
  //   });
  // }

  const formik = useFormik({
    initialValues: {
      username: '',
      password: '',
    },
    validationSchema,
    onSubmit: values => {
      console.log('values :: ', values);
      const { username, password } = values;
      Parse.User.logIn(username, password).then(r => {
        if (!_isEmpty(r.getSessionToken())) {
          props.history.push('/');
        } else {
          // TODO: show login error, authentication failed
        }
      });
    },
  });

  const { errors, values, handleSubmit, handleChange } = formik;

  // console.log('LoginPage :: props :: ', props, formik);

  return (
    <Grid container component="main" className={classes.root}>
      <CssBaseline />
      <Grid item xs={false} sm={4} md={7} className={classes.image} />
      <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
        <div className={classes.paper}>
          <Avatar className={classes.avatar}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Sign in
          </Typography>
          <form className={classes.form} onSubmit={handleSubmit}>
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              error={_has(errors, 'username')}
              label={errors.username ? errors.username : 'Email Address'}
              name="username"
              autoComplete="email"
              autoFocus
              value={values.username}
              onChange={handleChange}
              // onChange={e => setUName(e.target.value)}
            />
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              name="password"
              error={_has(errors, 'password')}
              label={errors.password ? errors.password : 'Password'}
              type="password"
              id="password"
              autoComplete="current-password"
              value={values.password}
              onChange={handleChange}
              // onChange={e => setPwd(e.target.value)}
            />
            <FormControlLabel
              control={<Checkbox value="remember" color="primary" />}
              label="Remember me"
              value={values.rememberMe}
              onChange={handleChange}
            />
            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
            >
              Sign In
            </Button>
            <Grid container>
              <Grid item xs>
                <Link href="#" variant="body2">
                  Forgot password?
                </Link>
              </Grid>
              <Grid item>
                <Link href="#" variant="body2">
                  {"Don't have an account? Sign Up"}
                </Link>
              </Grid>
            </Grid>
            <Box mt={5}>
              <CopyRight />
            </Box>
          </form>
        </div>
      </Grid>
    </Grid>
  );
}

LoginPage.propTypes = {
  history: PropTypes.object.isRequired,
  // dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  loginPage: makeSelectLoginPage(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(LoginPage);
