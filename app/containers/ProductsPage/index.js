/**
 *
 * ProductsPage
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import Paper from '@material-ui/core/Paper';
import { Divider } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';

import VirtualizedList from 'components/VirtualizedList';
import makeSelectProductsPage from './selectors';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';
const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(3, 2),
  },
}));
export function ProductsPage() {
  useInjectReducer({ key: 'productsPage', reducer });
  useInjectSaga({ key: 'productsPage', saga });
  const classes = useStyles();

  return (
    <Paper className={classes.root}>
      <Helmet>
        <title>Product Items</title>
        <meta name="description" content="Production Items" />
      </Helmet>
      <FormattedMessage {...messages.header} />

      <Divider />
      <VirtualizedList />
    </Paper>
  );
}

ProductsPage.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  productsPage: makeSelectProductsPage(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(ProductsPage);
