/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 *
 */

import React from "react";
import _map from "lodash/map";
// import { FormattedMessage } from 'react-intl';
// import DS from 'utils/ds';
// import messages from './messages';
import {
  BarChart,
  Bar,
  // Cell,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend
} from "recharts";

import IconButton from "@material-ui/core/IconButton";
import ReloadIcon from "mdi-material-ui/Reload";

// const data = [
//   {
//     date: "2016-12-31T18:30:00.000Z",
//     impressions: "2764609",
//     clicks: "3627",
//     revenue: "13092.1234790000000"
//   },
//   {
//     date: "2017-01-01T18:30:00.000Z",
//     impressions: "943070",
//     clicks: "1489",
//     revenue: "4275.3479640000000"
//   },
//   {
//     date: "2017-01-02T18:30:00.000Z",
//     impressions: "962220",
//     clicks: "1274",
//     revenue: "4349.9616000000000"
//   },
//   {
//     date: "2017-01-03T18:30:00.000Z",
//     impressions: "948574",
//     clicks: "1311",
//     revenue: "4364.3495500000000"
//   },
//   {
//     date: "2017-01-04T18:30:00.000Z",
//     impressions: "952714",
//     clicks: "1210",
//     revenue: "4496.4799380000000"
//   },
//   {
//     date: "2017-01-05T18:30:00.000Z",
//     impressions: "1122032",
//     clicks: "1473",
//     revenue: "4733.6558360000000"
//   },
//   {
//     date: "2017-01-06T18:30:00.000Z",
//     impressions: "1115322",
//     clicks: "1547",
//     revenue: "4644.1067680000000"
//   }
// ];

export default function HomePage() {
  const [data, setData] = React.useState([]);
  // function handlerStandUp(...args) {
  //   console.log('currentStandUp :: data ::', args);
  // }

  // function emitEvent() {
  //   console.log('currentStandUp :: emitting ::');
  //   DS.event.emit('currentStandUp', { time: Date.now() });
  // }

  // React.useEffect(() => {
  //   console.log('subscribing :: currentStandUp');
  //   DS.event.subscribe('currentStandUp', handlerStandUp);
  //   return () => {
  //     console.log('unsubscribing :: currentStandUp');
  //     DS.event.unsubscribe('currentStandUp', handlerStandUp);
  //   };
  // }, []);

  React.useEffect(() => {
    loadData();
  }, []);

  function loadData() {
    fetch("api/stats/daily")
      .then(response => response.json())
      .then(json => {
        // console.log("output", json);
        setData(json);
      });
  }

  return (
    <div>
      {/* <h1>
      <FormattedMessage {...messages.header} />
      <button type="button">
        <button type="button" onClick={emitEvent}> 
        Click me
      </button>
    </h1> */}
      <IconButton aria-label="refersh" type="button" onClick={loadData} >
        <ReloadIcon />
      </IconButton>
      <BarChart
        width={500}
        height={300}
        data={_map(data, v => ({ ...v, impressions: v.impressions / 1000 }))}
        margin={{
          top: 5,
          right: 30,
          left: 20,
          bottom: 5
        }}
      >
        <CartesianGrid strokeDasharray="3 3" />
        <XAxis dataKey="date" />
        <YAxis />
        <Tooltip />
        <Legend />
        <Bar dataKey="impressions" fill="#8884d8" />
        <Bar dataKey="clicks" fill="red" />
        <Bar dataKey="revenue" fill="#82ca9d" />
      </BarChart>
    </div>
  );
}
