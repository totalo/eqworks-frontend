/**
 *
 * App.js
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 *
 */

import React from 'react';
// import PropTypes from 'prop-types';

// import { compose } from 'redux';
// import { connect } from 'react-redux';
// import { createStructuredSelector } from 'reselect';

import { Switch, Route, useHistory } from 'react-router-dom';

// custom containers
import LoginPage from 'containers/LoginPage/Loadable';
import HomePage from 'containers/HomePage/Loadable';
import ProductsPage from 'containers/ProductsPage/Loadable';
import NotFoundPage from 'containers/NotFoundPage/Loadable';

// custom components
import PrivateRoute from 'components/PrivateRoute/Loadable';
import LayoutTemplate from 'components/LayoutTemplate';

import Parse from 'utils/parse';

import GlobalStyle from '../../global-styles';

// const scrumBoard992993 = DS.record.getRecord('scrumBoard/992993');
// console.log('scrumBoard992993', scrumBoard992993)

export default function App() {
  const history = useHistory();

  function onMenuClick(menuId) {
    // console.log('onMenuClick ::', menuId);
    switch (menuId) {
      case 'secondary.menu.logOut':
        // console.log('logout clicked :: ', history);
        Parse.User.logOut().then(() => {
          // console.log('logout response:: ', r);
          history.push('/login');
        });

        break;

      default:
        break;
    }
  }
  return (
    <>
      <GlobalStyle />
      <Switch>
        <Route exact key="app.login" path="/login" component={LoginPage} />
        <PrivateRoute routeOnFail="/login">
          <LayoutTemplate onMenuClick={onMenuClick}>
            <Route exact key="app.home" path="/" component={HomePage} />
            <Route
              exact
              key="app.products"
              path="/products"
              component={ProductsPage}
            />
          </LayoutTemplate>
        </PrivateRoute>
        <Route component={NotFoundPage} />
      </Switch>
    </>
  );
}

App.propTypes = {
  // history: PropTypes.object.isRequired,
  // dispatch: PropTypes.func.isRequired,
};

// const mapStateToProps = createStructuredSelector({
//   // loginPage: makeSelectLoginPage(),
// });

// function mapDispatchToProps(dispatch) {
//   return {
//     dispatch,
//   };
// }

// const withConnect = connect(
//   mapStateToProps,
//   mapDispatchToProps,
// );
// export default compose(withConnect)(App);
