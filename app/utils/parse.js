const Parse = require('parse');

Parse.initialize('TOTALO_APP');
Parse.serverURL = 'https://clinic.totalo.in/api';
// Parse.liveQueryServerURL = 'ws://clinic.totalo.in/api';

export default Parse;
