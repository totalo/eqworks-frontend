/**
 *
 * ListMenuItem
 *
 */

import React, { memo } from 'react';
import PropTypes from 'prop-types';
// import styled from 'styled-components';
import { Link as RouterLink } from 'react-router-dom';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';

function ListMenuItem({
  isButton,
  id,
  icon: IconLoaded,
  caption,
  onMenuClick,
  navigateToLink,
}) {
  const routeProps = navigateToLink && {
    to: navigateToLink,
    component: RouterLink,
  };

  return (
    <ListItem button={isButton} onClick={() => onMenuClick(id)} {...routeProps}>
      <ListItemIcon>
        <IconLoaded />
      </ListItemIcon>
      <ListItemText primary={caption} />
    </ListItem>
  );
}

ListMenuItem.propTypes = {
  isButton: PropTypes.bool,
  icon: PropTypes.object,
  id: PropTypes.string,
  caption: PropTypes.string,
  onMenuClick: PropTypes.func,
  navigateToLink: PropTypes.string,
};

export default memo(ListMenuItem);
