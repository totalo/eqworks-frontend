/*
 * DashBoard Messages
 *
 * This contains all the text for the DashBoard component.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.components.DashBoard';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the DashBoard component!',
  },
});
