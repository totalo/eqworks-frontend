/*
 * CopyRight Messages
 *
 * This contains all the text for the CopyRight component.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.components.CopyRight';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the CopyRight component!',
  },
});
