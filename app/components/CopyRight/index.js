/**
 *
 * CopyRight
 *
 */

import React, { memo } from 'react';
// import PropTypes from 'prop-types';
// import styled from 'styled-components';
// import { FormattedMessage } from 'react-intl';
// import messages from './messages';
import Typography from '@material-ui/core/Typography';
import Link from '@material-ui/core/Link';

function CopyRight() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Copyright © '}
      <Link color="inherit" href="https://resume.totalo.in/">
        Totalo
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

CopyRight.propTypes = {};

export default memo(CopyRight);
