/*
 * FixedSizeList Messages
 *
 * This contains all the text for the FixedSizeList component.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.components.FixedSizeList';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the FixedSizeList component!',
  },
});
