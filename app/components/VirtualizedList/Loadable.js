/**
 *
 * Asynchronously loads the component for FixedSizeList
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
