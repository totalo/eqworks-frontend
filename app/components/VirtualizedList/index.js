/**
 *
 * VirtualizedList
 *
 */

import React, { memo } from 'react';
import PropTypes from 'prop-types';

import { FixedSizeList } from 'react-window';

import { makeStyles } from '@material-ui/core/styles';

import Box from '@material-ui/core/Box';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles(() => ({
  // root: {
  //   width: '100%',
  //   height: 400,
  //   // maxWidth: 360,
  //   backgroundColor: theme.palette.background.paper,
  // },
  inline: {
    display: 'inline',
  },
}));

function Row(props) {
  const { index, style } = props;
  const classes = useStyles();

  return (
    <>
      <ListItem alignItems="flex-start" style={style} key={index}>
        <ListItemText
          primary={`Item ${index + 1}`}
          secondary={
            <React.Fragment>
              <Typography
                component="span"
                variant="body2"
                className={classes.inline}
                color="textPrimary"
              >
                Ali Connors
              </Typography>
              {" — I'll be in your neighborhood doing errands this…"}
            </React.Fragment>
          }
        />
      </ListItem>
      <Divider  />
    </>
  );
}

Row.propTypes = {
  index: PropTypes.number.isRequired,
  style: PropTypes.object.isRequired,
};

function VirtualizedList(props) {
  console.group('VirtualizedList :: render');
  console.log('props :: ', props);
  console.groupEnd();

  return (
    <Box width={1}>
      {/* <div className={classes.root}> */}
      <FixedSizeList height={500}  itemSize={46} itemCount={200}>
        {Row}
      </FixedSizeList>
    </Box>
  );
}

VirtualizedList.propTypes = {};

export default memo(VirtualizedList);
