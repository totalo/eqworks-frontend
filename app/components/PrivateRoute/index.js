/**
 *
 * PrivateRoute
 *
 */

import React from 'react';
// import PropTypes from 'prop-types';
// import styled from 'styled-components';
import { Redirect } from 'react-router-dom';

import Parse from 'utils/parse';

// import { FormattedMessage } from 'react-intl';
// import messages from './messages';

function PrivateRoute({ children, routeOnFail = '/login' }) {
  // console.log('ProtectedRoutes :: auth ::', auth);
  const userAuthenticated = Parse.User.current()
    ? Parse.User.current().authenticated()
    : false;
  if (!userAuthenticated) {
    return <Redirect push to={routeOnFail} />;
  }
  // return <Redirect push to="/" />;
  return children;
}

PrivateRoute.propTypes = {};

export default PrivateRoute;
