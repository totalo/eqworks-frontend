/**
 *
 * Asynchronously loads the component for DrawerMenuListItems
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
