/*
 * DrawerMenuListItems Messages
 *
 * This contains all the text for the DrawerMenuListItems component.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.components.DrawerMenuListItems';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the DrawerMenuListItems component!',
  },
});
