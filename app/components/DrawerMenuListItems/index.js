/**
 *
 * DrawerMenuListItems
 *
 */

import React, { memo } from 'react';
import PropTypes from 'prop-types';
// import styled from 'styled-components';

import List from '@material-ui/core/List';

// import ListSubheader from '@material-ui/core/ListSubheader';
import Divider from '@material-ui/core/Divider';

// custom-components
import ListMenuItem from 'components/ListMenuItem';

// icons
import ViewDashboard from 'mdi-material-ui/ViewDashboard';
import PackageVariantClosed from 'mdi-material-ui/PackageVariantClosed';
import Cart from 'mdi-material-ui/Cart';
import ChartBar from 'mdi-material-ui/ChartBar';
import AccountGroup from 'mdi-material-ui/AccountGroup';
import ExitToApp from 'mdi-material-ui/ExitToApp';

// import { FormattedMessage } from 'react-intl';

// import messages from './messages';

// https://materialdesignicons.com/
const primaryMenuItems = [
  {
    isButton: true,
    icon: ViewDashboard,
    id: 'primary.menu.dashboard',
    caption: 'Dashboard',
    navigateToLink: '/',
  },
  {
    isButton: true,
    icon: PackageVariantClosed,
    id: 'primary.menu.products',
    caption: 'Products',
    navigateToLink: '/products',
  },
  {
    isButton: true,
    icon: Cart,
    id: 'primary.menu.orders',
    caption: 'Orders',
    navigateToLink: '/orders',
  },
  {
    isButton: true,
    icon: ChartBar,
    id: 'primary.menu.customer',
    caption: 'Customers',
    navigateToLink: '/customers',
  },
  {
    isButton: true,
    icon: AccountGroup,
    id: 'primary.menu.reports',
    caption: 'Reports',
    navigateToLink: '/reports',
  },
];
const secondaryMenuItems = [
  {
    isButton: true,
    icon: ExitToApp,
    id: 'secondary.menu.logOut',
    caption: 'Log Out',
  },
];

// <FormattedMessage {...messages.header} />
function DrawerMenuListItems({ onMenuClick }) {
  return (
    <>
      <List>
        <For each="menuItem" index="idx" of={primaryMenuItems}>
          <ListMenuItem key={idx} {...menuItem} onMenuClick={onMenuClick} />
        </For>
      </List>
      <Divider />
      <List>
        <For each="menuItem" index="idx" of={secondaryMenuItems}>
          <ListMenuItem key={idx} {...menuItem} onMenuClick={onMenuClick} />
        </For>
      </List>
    </>
  );
}

DrawerMenuListItems.propTypes = {
  onMenuClick: PropTypes.func,
};

export default memo(DrawerMenuListItems);
